#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup as bs
import requests
import time
import json
from tkinter import *
import tkinter.ttk as ttk
import threading
from multiprocessing import Queue

URL = 'https://dkhp.hcmue.edu.vn/'
LOGIN_ROUTE = 'Login'

# URL2 = 'https://dkmh.huflit.edu.vn/'
# DKHP_ROUTE = 'DangKyHocPhan'

HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0',
           'origin': URL, 'referer': URL + LOGIN_ROUTE}

# HEADERS_1 = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0',
#            'origin': URL2, 'referer': URL2 + DKHP_ROUTE}

login_error = "Gặp lỗi chưa đăng nhập được"
time_error = "Chưa đến thời hạn đăng ký học phần"
wrong_error = "Sai tài khoản, mật khẩu! Vui lòng update lại trong file codeclass.txt"


# class ThreadedTask(threading.Thread):
#     def __init__(self, queue):
#         threading.Thread.__init__(self)
#         q = queue
#         self.daemon = True
#         self.name = "thread1"

#     def run(self):
#         # onclick()
#         q.put("Task finished")


# q = Queue()


class Example(ttk.Frame):
    def __init__(self, parent):
        ttk.Frame.__init__(self, parent)
        self.parent = parent
        self.initUI()

    def initUI(self):
        # frame chinh
        self.pack(fill=BOTH, expand=True)
        frame = ttk.Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=True)
        self.parent.title("Tool đăng kí học phần")
        self.style = ttk.Style()
        self.style.theme_use("clam")
        self.pack(fill=BOTH, expand=1)

        # frame 1
        frame1 = ttk.Frame(frame)
        frame1.pack(fill=X)

        # Label MSSV
        lblMSSV = ttk.Label(frame1, text="MSSV", width=10)
        lblMSSV.pack(side=LEFT, padx=5, pady=5)

        # Entry MSSV
        self.entryMSSV = ttk.Entry(frame1, width=30)
        self.entryMSSV.pack(side=LEFT, padx=5)

        # Label password
        lblPassword = ttk.Label(frame1, text="password", width=10)
        lblPassword.pack(side=LEFT, padx=5, pady=5)

        # Entry rest
        self.entryPass = ttk.Entry(frame1, width=30)
        self.entryPass.pack(side=LEFT, padx=5)

        # Label rest
        lblRest = ttk.Label(frame1, text="Time res", width=10)
        lblRest.pack(side=LEFT, padx=5, pady=5)

        # Entry rest
        self.entryRest = ttk.Entry(frame1, width=30)
        self.entryRest.pack(side=LEFT, padx=5)

        # Check File Button
        self.chk_state = IntVar()
        self.chk_state.set(0)
        self.chkButton = Checkbutton(
            frame1, text="Nhập từ file codeclass.txt", var=self.chk_state, onvalue=1, offvalue=0)
        self.chkButton.pack(fill=X, padx=5, expand=True)

        # Frame 2
        frame2 = ttk.Frame(frame)
        frame2.pack(fill=X)

        # Frame 3
        frame3 = ttk.Frame(frame)
        frame3.pack(fill=X)

        # Label class
        lbl3 = ttk.Label(frame3, text="Môn học", width=10)
        lbl3.pack(side=LEFT, anchor=N, padx=5, pady=5)

        # Entry class
        self.entryClass = ttk.Entry(frame3, width=100)
        self.entryClass.pack(fill=X, padx=5, expand=True)

        # Frame 4
        frame4 = ttk.Frame(frame)
        frame4.pack(fill=X)

        # Label log
        lbl3 = ttk.Label(frame4, text="Log", width=10)
        lbl3.pack(side=LEFT, anchor=N, padx=5, pady=5)

        # Text log
        scrollbar = Scrollbar(frame4)
        scrollbar.pack(side=RIGHT, fill=Y)
        self.textbox = Text(frame4)
        self.textbox.pack(fill=BOTH, pady=5, padx=5, expand=True)
        self.textbox.config(yscrollcommand=scrollbar.set)
        self.textbox.configure(font=("Courier", 8, "italic"))
        scrollbar.config(command=self.textbox.yview)
        self.textbox.tag_configure('error', foreground='red')
        self.textbox.tag_configure('warning', foreground='blue')
        self.textbox.tag_configure('success', foreground='green')

        # Frame 2
        framef = ttk.Frame(self)
        framef.pack(fill=BOTH, expand=False)

        # button
        self.button = ttk.Button(framef)
        self.button.pack(side=RIGHT, padx=5, pady=5)

        # Button delete log
        self.delete = ttk.Button(framef, text="Xoá log",
                                 command=self.deleteTextbox)
        self.delete.pack(side=RIGHT, padx=5, pady=5)

        # create progress
        self.progress()

        # create function for button
        self._resetbutton()

    # def stop_click(self):
    #     self.stop["state"] = "disable"
    #     self.button["state"] = "normal"
    #     self.prog_bar.stop()
    #     q.put("Task fall")

    def deleteTextbox(self):
        self.textbox['state'] = 'normal'
        self.textbox.delete('1.0', END)
        self.textbox['state'] = 'disable'

    def progress(self):
        self.prog_bar = ttk.Progressbar(
            self.master, orient="horizontal",
            length=1000, mode="indeterminate"
        )
        self.prog_bar.pack(side=TOP)

    # def tb_click(self):
    #     # self.progress()
    #     self.prog_bar.start()
    #     self.button["state"] = "disable"
    #     self.stop["state"] = "normal"
    #     # self.queue = Queue()
    #     self.t1 = ThreadedTask(q).start()
    #     self.master.after(100, self.process_queue)

    # def process_queue(self):
    #     if q.empty() == False:
    #         msg = q.get(0)
    #         print(msg)
    #         self.prog_bar.stop()
    #         self.button["state"] = "normal"
    #     # Show result of the task if needed
    # # except Queue.Empty:
    #     else:
    #         self.master.after(100, self.process_queue)

    def _resetbutton(self):
        self.running = False
        self.prog_bar.stop()
        self.button.config(text="Đăng kí học phần",
                           command=self.startthread)

    def startthread(self):
        self.running = True
        self.prog_bar.start()
        newthread = threading.Thread(target=self.onclick, daemon=True)
        newthread.start()
        self.button.config(text="Dừng", command=self._resetbutton)

    def onclick(self):
        try:
            if self.chk_state.get() == 0:
                try:
                    username = self.entryMSSV.get()
                    password = self.entryPass.get()
                    time_refresh_login = float(self.entryRest.get())
                    code_class = self.entryClass.get()
                    code_class = code_class.split("|")
                except:
                    self.textbox['state'] = 'normal'
                    self.textbox.insert(
                        END, f"Bạn điền thông tin sai\n", "error")
                    self.textbox.see(END)
                    self.textbox['state'] = 'disable'
                    self._resetbutton()
                    return
            else:
                try:
                    file = open("codeclass.txt", "r")
                    # Mo file cai dat
                    username = file.readline()
                    password = file.readline()
                    time_refresh_login = float(file.readline())
                    code_class = file.readline()
                    code_class = code_class.split("|")
                except:
                    self.textbox['state'] = 'normal'
                    self.textbox.insert(
                        END, f"Nhập từ file bị sai\n", "error")
                    self.textbox.see(END)
                    self.textbox['state'] = 'disable'
                    self._resetbutton()
                    return
            s = requests.session()
            login_payload = {
                'username': username,
                'password': password,
            }
            # Thiet lap payload cho dang nhap

            number_of_courses_registered_failed = 0
            # Hoc phan dang ki that bai
            course_registration_failed = []
            # List cac hoc phan dang ki that bai
            number_of_courses_registered_success = 0
            # Hoc phan dang ki thanh cong
            course_registration_success = []
            count_error = 0
            while self.running:
                login_req = s.post(URL + LOGIN_ROUTE, headers=HEADERS, data=login_payload)
                dkhp_request = s.post(URL2 + DKHP_ROUTE, headers=HEADERS, data=login_payload)
                time.sleep(time_refresh_login)
                # print(time_refresh_login)
                if login_req.status_code != 200:
                    count_error += 1
                    self.textbox['state'] = 'normal'
                    self.textbox.insert(
                        END, f"{login_error} {login_req.status_code} lần thử {count_error}\n", "error")
                    self.textbox.see(END)
                    self.textbox['state'] = 'disable'
                elif time_error in login_req.text:
                    count_error += 1
                    self.textbox['state'] = 'normal'
                    self.textbox.insert(
                        END, f"{time_error} {login_req.status_code} lần thử {count_error}\n", "error")
                    self.textbox.see(END)
                    self.textbox['state'] = 'disable'
                elif "Đăng xuất" in login_req.text:
                    break
                elif "không đúng" in login_req.text:
                    self.textbox['state'] = 'normal'
                    self.textbox.insert(END, f"{wrong_error}\n", "error")
                    self.textbox.see(END)
                    self.textbox['state'] = 'disable'
                    self._resetbutton()
                    return
                else:
                    count_error += 1
                    self.textbox['state'] = 'normal'
                    self.textbox.insert(
                        END, f"Gặp lỗi đang thử lại {login_req.status_code} lần thử {count_error}\n", "error")
                    self.textbox.see(END)
                    self.textbox['state'] = 'disable'
            if not self.running:
                self.textbox['state'] = 'normal'
                self.textbox.insert(
                    END, f"Đăng nhập bị buộc dừng bởi thao tác người dùng\n", "warning")
                self.textbox.see(END)
                self.textbox['state'] = 'disable'
                self._resetbutton()
                return
            self.textbox['state'] = 'normal'
            self.textbox.insert(END, f"Đăng nhập thành công OK\n", "success")
            self.textbox.see(END)
            self.textbox['state'] = 'disable'
            cookies = login_req.cookies
            # luu cookies dang nhap

            # type_class = file.readline()
            # type_class = type_class.split("|")

            register_payload = {
                'Hide': '',
                'acceptConflict': 'false',
                'classStudyUnitConflictId': '',
                'RegistType': ''
            }
            # thiet lap payload cho dang ki

            for i in range(len(code_class)):
                temp_register_payload = register_payload
                temp_register_payload["Hide"] = code_class[i][0:14] + \
                    "$0.0$" + code_class[i][0:12] + "$$0|"
                while self.running:
                    register = s.get(URL + "DangKyHocPhan/DangKy",
                                     params=temp_register_payload, timeout=60)
                    if register.status_code == 200:
                        if "Đăng ký thành công" in register.text or "Lớp học phần này đã đăng ký" in register.text:
                            self.textbox['state'] = 'normal'
                            self.textbox.insert(
                                END, f"Đăng ký thành công học phần {code_class[i][0:14]} OK\n", "success")
                            self.textbox.see(END)
                            self.textbox['state'] = 'disable'
                            number_of_courses_registered_success += 1
                            course_registration_success.append(code_class[i])
                            break
                        elif "Trùng lịch:" in register.json()['Msg']:
                            temp_register_payload['acceptConflict'] = 'true'
                            temp_register_payload['classStudyUnitConflictId'] = code_class[i][0:14]
                            count_error += 1
                            self.textbox['state'] = 'normal'
                            self.textbox.insert(
                                END, f"Gặp lỗi đang thử lại!! {register.json()['Msg']} {count_error}\n", "error")
                            self.textbox.see(END)
                            self.textbox['state'] = 'disable'
                        elif "đủ số lượng" in register.text:
                            self.textbox['state'] = 'normal'
                            self.textbox.insert(
                                END, f"Học phần đủ số lượng, chuyển qua đăng kí học phần khác!!\n", "warning")
                            self.textbox.see(END)
                            self.textbox['state'] = 'disable'
                            number_of_courses_registered_failed += 1
                            course_registration_failed.append(code_class[i])
                            break
                        else:
                            count_error += 1
                            self.textbox['state'] = 'normal'
                            self.textbox.insert(
                                END, f"Gặp lỗi đang thử lại!! {register.json()['Msg']} {count_error}\n", "error")
                            self.textbox.see(END)
                            self.textbox['state'] = 'disable'
                    else:
                        count_error += 1
                        self.textbox['state'] = 'normal'
                        self.textbox.insert(
                            END, f"Gặp lỗi đang thử lại {register.status_code} {count_error}\n", "error")
                        self.textbox.see(END)
                        self.textbox['state'] = 'disable'
                    time.sleep(1)

            self.textbox['state'] = 'normal'
            self.textbox.insert(
                END, f"Đã đăng kí xong OK, {number_of_courses_registered_failed} học phần đã full, {number_of_courses_registered_success} học phần thành công\n", "error")
            self.textbox.insert(END, f"Các học phần thất bại: ", "error")
            for i in course_registration_failed:
                self.textbox.insert(END, f"{i[0:14]} ", "error")
            self.textbox.insert(END, f"\n")
            self.textbox.insert(END, f"Các học phần thành công: ", "success")
            for i in course_registration_success:
                self.textbox.insert(END, f"{i[0:14]} ", "success")
            self.textbox.see(END)
            self.textbox['state'] = 'disable'
            self._resetbutton()
        except:
            self._resetbutton()


root = Tk()
root.geometry("1000x500+0+0")
app = Example(root)
root.mainloop()
